**Welcome to my Coding Sample Portfolio!**

Hello!
My name is Zhuohong He. I am an undergraduate student at Johns Hopkins University where I am majoring in Mechanical Engineering and minoring in Computer Science. 

Although CS is not my major, I have a strong background in coding. I am proficient in Java, Python, C/C++, MATLAB, and Arduino.

This repository contains samples of my coding work in the past few years. Click into each folder for detailed descriptions of the project.

**Contents:**

1. **Trace** Stall Tracking Application - Volvo Internship - JAVA, SQL, MS Azure, Raspberry Pi, JavaFx
2. **Smart Cabinet** Filing Application - Written for ArtSkills Inc - JAVA, SQL