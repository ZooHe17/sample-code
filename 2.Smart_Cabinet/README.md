## Smart Cabinet Filing Application
**Written for ArtSkills Inc. - Jan 2019**


**The Problem:** ArtSkills is a consumer products manufacturer based in Bethlehem, PA. They were pushing an initiative to move towards a paperless filing system for their Accounting and Operations Departments. They hired me to write a user friendly, secure, and customized program to fit their needs. 

**My Solution:** I built a JavaFX filing application that stores invoice and payment documents, OCR recognizes key information about the document (such as check numbers and invoice IDs), saves information into a SQLite database, and searches and extracts stored documents based on key information.

**The Result** After several employee trainings, the company currently files over 30,000 pdf documents a year with my program. I placed my SQL database and code on the company server, so multiple employees can operate the program simultaneously.
