/**
 * Zhuohong (Zooey) He
 * zhe@artskills.com
 * Smart Cabinet Project - January 10th, 2019
 *
 * This class reads files invoice documents for critical information.
 */

package sample;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.bytedeco.javacv.LeptonicaFrameConverter;

import java.awt.image.BufferedImage;

public class ocrDriver {
    int[] invNumBx = new int[4];
    int[] invPoBx = new int[4];
    int[] invCustBx = new int[4];
    int[] invDtBx = new int[4];


    /**
     * Constructor class, initializes locations where OCR will be used.
     * @param width integer image width
     * @param height integer image height
     */
    public ocrDriver(int width, int height) {
        int w = width;
        int h = height;
        invNumBx[0] = (int) Math.round(5.0/6 * w);
        invNumBx[1] = (int) Math.round(4.0/35 * h) - 2;
        invNumBx[2] = (int) Math.round(23.0/216 * w) + 2;
        invNumBx[3] = (int) Math.round(1.0/40 * h) + 2;

        invPoBx[0] = (int) Math.round(5.0/60 * w);
        invPoBx[1] = (int) Math.round(101.0/280 * h);
        invPoBx[2] = (int) Math.round(19.0/108 * w);
        invPoBx[3] = (int) Math.round(3.0/100 * h);

        invCustBx[0] = (int) Math.round(5.0/54 * w);
        invCustBx[1] = (int) Math.round(53.0/280 * h);
        invCustBx[2] = (int) Math.round(20.0/108 * w) + 1 ; //For some reason width cannot be odd (Go Figure)
        invCustBx[3] = (int) Math.round(3.0/140 * h);

        invDtBx[0] = (int) Math.round(79.0/115 * w);
        invDtBx[1] = (int) Math.round(4.0/35 * h);
        invDtBx[2] = (int) Math.round(7.0/60 * w) + 1;
        invDtBx[3] = (int) Math.round(1.0/40 * h);
    }


    /**
     * Applies OCR to sensitive areas of the input image. Scans for invoice
     * number, PO number, Customer, and Invoice Date. Also formats the input
     * @param img [BufferedImage] first page of invoice
     * @return String array with read information in order of [invoice num, po
     * num, customer, date]
     */
    public String[] readInvoice(BufferedImage img) {
        String[] n = new String[4];
        n[0] = numsOnly(readOCR(img, invNumBx[0], invNumBx[1], invNumBx[2], invNumBx[3]));
        n[1] = numAndLettersOnly(readOCR(img, invPoBx[0], invPoBx[1], invPoBx[2], invPoBx[3]));
        n[2] = lettersOnly(readOCR(img, invCustBx[0], invCustBx[1], invCustBx[2], invCustBx[3]));
        n[3] = dateOnly(readOCR(img, invDtBx[0], invDtBx[1], invDtBx[2], invDtBx[3]));
        return n;
    }

    /**
     * Applies OCR to read english text from a rectangular area of an image.
     * @param fullImg [BufferedImage] containing the full, original image
     * @param x [integer] x coord. of upper left corner of rectangular area.
     * @param y [integer] y coord. of upper left corner of rectangular area.
     * @param w [integer] width of the area.
     * @param h [integer] height of the area.
     * @return [string] result of the OCR read.
     */
    public String readOCR(BufferedImage fullImg, int x, int y, int w, int h) {
        String out;
        tesseract.TessBaseAPI api = new tesseract.TessBaseAPI();

        if (api.Init(null, "eng") != 0) {
            System.err.println("Could not initialize tesseract.");
            System.exit(1);
        }

        BufferedImage img = fullImg.getSubimage(x, y, w, h);
        Java2DFrameConverter converter = new Java2DFrameConverter();
        LeptonicaFrameConverter converter2 = new LeptonicaFrameConverter();

        lept.PIX pix = converter2.convert(converter.convert(img));
        lept.pixSetResolution(pix, 70, 70);

        api.SetImage(pix);

        BytePointer outBytes = api.GetUTF8Text();
        if (outBytes.isNull()) {
            return "Null, not read";
        }

        out = outBytes.getString();
        outBytes.deallocate();
        pix.destroy();
        api.End();
        return out;
    }


    /**
     * Replaces non-numbers with empty string ""
     * @param in input string
     * @return refactored string
     */
    public static String numsOnly(String in){
        String out = in.replaceAll("[^0-9]", "");
        return out;
    }

    /**
     * Only returns string parts that are letters and numbers
     * @param in unformatted string
     * @return formatted string
     */
    public static String numAndLettersOnly(String in) {
        String out = in.replaceAll("[^0-9a-zA-Z]", "");
        return out;
    }

    /**
     * Only returns substrings that pertain to dates (numbers and '/')
     * @param in unformatted string
     * @return formatted string
     */
    public static String dateOnly (String in) {
        String out = in.replaceAll("[^0-9/]", "");
        return out;
    }

    /**
     * Only returns substrings that are letters
     * @param in input string
     * @return formatted string
     */
    public static String lettersOnly (String in) {
        String out = in.replaceAll("[^a-zA-Z]", "");
        return out;
    }
}
