/**
 * Zhuohong (Zooey) He
 * zhe@artskills.com
 * Smart Cabinet Project - January 10th, 2019
 *
 * This class is used to store meta information on a single file.
 */
package sample;

import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;

public class Entry {
    String type; //invoice or payment
    SimpleStringProperty key = new SimpleStringProperty();
    SimpleStringProperty one = new SimpleStringProperty();
    SimpleStringProperty two = new SimpleStringProperty();
    SimpleStringProperty three = new SimpleStringProperty();
    SimpleStringProperty four = new SimpleStringProperty();
    SimpleStringProperty filename = new SimpleStringProperty();
    SimpleStringProperty address = new SimpleStringProperty();
    SimpleStringProperty filesize = new SimpleStringProperty();
    SimpleStringProperty subdate = new SimpleStringProperty();

    /**
     * Constructor
     * @param values ArrayList to base entries information off of.
     * @param t entry type [invoice] or [payment]
     */
    public Entry(ArrayList<String> values, String t) {
        type = t;
        key.set(values.get(0));
        filename.set(values.get(1));
        address.set(values.get(2));
        filesize.set(values.get(3));
        one.set(values.get(4));
        two.set(values.get(5));
        three.set(values.get(7));
        four.set(values.get(6));
        subdate.set(values.get(8));
    }

    // The following are getters and setters
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKey() {
        return key.get();
    }

    public SimpleStringProperty keyProperty() {
        return key;
    }

    public void setKey(String key) {
        this.key.set(key);
    }

    public String getOne() {
        return one.get();
    }

    public SimpleStringProperty oneProperty() {
        return one;
    }

    public void setOne(String one) {
        this.one.set(one);
    }

    public String getTwo() {
        return two.get();
    }

    public SimpleStringProperty twoProperty() {
        return two;
    }

    public void setTwo(String two) {
        this.two.set(two);
    }

    public String getThree() {
        return three.get();
    }

    public SimpleStringProperty threeProperty() {
        return three;
    }

    public void setThree(String three) {
        this.three.set(three);
    }

    public String getFour() {
        return four.get();
    }

    public SimpleStringProperty fourProperty() {
        return four;
    }

    public void setFour(String four) {
        this.four.set(four);
    }

    public String getFilename() {
        return filename.get();
    }

    public SimpleStringProperty filenameProperty() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename.set(filename);
    }

    public String getAddress() {
        return address.get();
    }

    public SimpleStringProperty addressProperty() {
        return address;
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public String getFilesize() {
        return filesize.get();
    }

    public SimpleStringProperty filesizeProperty() {
        return filesize;
    }

    public void setFilesize(String filesize) {
        this.filesize.set(filesize);
    }

    public String getSubdate() {
        return subdate.get();
    }

    public SimpleStringProperty subdateProperty() {
        return subdate;
    }

    public void setSubdate(String subdate) {
        this.subdate.set(subdate);
    }
}
