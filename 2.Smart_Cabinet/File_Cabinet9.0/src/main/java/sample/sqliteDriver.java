/**
 * Zhuohong (Zooey) He
 * zhe@artskills.com
 * Smart Cabinet Project - January 10th, 2019
 */
package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

import static sample.Controller.lineNumber;

public class sqliteDriver {

    Connection conn;

    /**
     * Constructor for SQLite Driver.
     */
    public sqliteDriver() {
        String url = "";
        try {
            url = new File(Main.class.getProtectionDomain().getCodeSource().getLocation()
                    .toURI()).getParent() + "\\config\\contents.db";
        } catch (Exception e) {
            e.printStackTrace();
        }
        url = "jdbc:sqlite:" + url;

        try {
            conn = DriverManager.getConnection(url);
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("....Connection to database " + meta.getDriverName() + " established....");
            }
        } catch (SQLException e) {
            System.err.println("Error @ " + lineNumber() + ": Error making Connection");
            e.printStackTrace();
        }
    }

    /**
     * Constructor for SQLite driver
     * @param address path to the database.
     */
    public sqliteDriver(String address) {
        String url = "jdbc:sqlite:" + address;
        try {
            conn = DriverManager.getConnection(url);
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("....Connection to database " + meta.getDriverName() + " established....");
            }
        } catch (SQLException e) {
            System.err.println("Error @ " + lineNumber() + ": Error making Connection");
        }
    }

    /**
     * Queries a statement into the database.
     * TODO: If have time, convert to prepared statement for security reasons.
     * @param inputStatement string query statement
     * @return returns an observable list with the results.
     */
    public ObservableList<Entry> query(String inputStatement) {
        ObservableList<Entry> ret = FXCollections.observableArrayList();
        try {
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(inputStatement);
            ResultSetMetaData meta = rs.getMetaData();
            while (rs.next()) {
                ArrayList<String> arr = new ArrayList<>();
                for (int i = 1; i <= meta.getColumnCount(); i++) {
                    arr.add(rs.getString(i));
                }
                Entry e = new Entry(arr, meta.getTableName(1));
                ret.add(e);
            }
        } catch (SQLException e) {
            System.err.println("Error querying statement: " + inputStatement);
        }
        return ret;
    }

    /**
     * Queries a statement into the database.
     * TODO: If have time, convert to prepared statement for security reasons
     * @param inputStatement string query statement
     * @return returns an array list with results
     */
    public ArrayList<String[]> queryArrList(String inputStatement) {
        ArrayList<String[]> arr = new ArrayList<>();
        try {
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(inputStatement);
            ResultSetMetaData meta = rs.getMetaData();
            while (rs.next()) {
                String[] s = new String[meta.getColumnCount()];
                for (int i = 0; i < meta.getColumnCount(); i++) {
                    s[i] = rs.getString(i + 1);
                }
                arr.add(s);
            }
        } catch (SQLException e) {
            System.err.println("Error querying statment: @" + Controller.lineNumber() + " " + inputStatement);
            e.printStackTrace();
        }
        return arr;
    }

    /**
     * Execute a string. Used to modify, update, or add to the database
     * @param exe This is the statement we would like to execute.
     * @return
     */
    public boolean execute(String exe) {
        try {
            Statement statement = conn.createStatement();
            statement.execute(exe);
        } catch (SQLException e){
            System.err.println("Unable to execute: " + exe);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Running this main (without the wizard) allows user to quickly set up a
     * correctly formatted database. The wizard helps create custom tables.
     * @param args command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Do you want to run the wizard?");
        Scanner scan = new Scanner(System.in);
        String ans = scan.next();
        String url = "";
        try {
            url = new File(Main.class.getProtectionDomain().getCodeSource().getLocation()
                    .toURI()).getParent() + "\\config\\contents.db";
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ans.charAt(0) == 'y' || ans.charAt(0) == 'Y') {
            wizard();
        } else {
            sqliteDriver s = new sqliteDriver(url);
            String exeStr = "CREATE TABLE invoices (key INTEGER PRIMARY KEY, " +
                    "filename VARCHAR(3000) NOT NULL, address VARCHAR(3000) NOT NULL, " +
                    "filesize INTEGER, inv_num VARCHAR(300) NOT NULL, " +
                    "po_num VARCHAR(300) NOT NULL, cust VARCHAR(300) NOT NULL, " +
                    "inv_date DATE, " +
                    "sub_date DATE);";

            s.execute(exeStr);

            exeStr = "CREATE TABLE payments (key INTEGER PRIMARY KEY, " +
                    "filename VARCHAR(3000) NOT NULL, address VARCHAR(3000) NOT NULL, " +
                    "filesize INTEGER, chk_num VARCHAR(300) NOT NULL, " +
                    "amt_num VARCHAR(300) NOT NULL, cust VARCHAR(300) NOT NULL, " +
                    "dep_date DATE, " +
                    "sub_date DATE);";

            s.execute(exeStr);

            exeStr = "CREATE TABLE settings (key INTEGER PRIMARY KEY, " +
                    "setting VARCHAR(3000) NOT NULL, value VARCHAR(3000) NOT NULL);";

            s.execute(exeStr);
            s.close();
        }
        scan.close();
    }

    /**
     * This is the database creation wizard. It helps set up the database
     * on first use.
     */
    private static void wizard() {
        System.out.println("This wizard is used to create a sql database");
        System.out.println("Location of the database:");
        Scanner scan = new Scanner(System.in);
        String address = scan.next();
        System.out.println("Database Name: ");
        address += scan.next();
        sqliteDriver s = new sqliteDriver(address);

        System.out.println("Do you want to create a Table?");
        String answer = scan.next();
        while (answer.charAt(0) == 'Y' || answer.charAt(0) == 'y') {
            System.out.println("Table Name:");
            answer = scan.next();
            String exe = "CREATE TABLE " + answer + " (";
            System.out.println("Do you want to add any columns?");
            answer = scan.next();
            while (answer.charAt(0) == 'Y' || answer.charAt(0) == 'y') {
                System.out.println("Column Name: ");
                answer = scan.next();
                exe = exe + answer + " ";
                System.out.println("Column Type Data: ");
                scan.nextLine();
                answer = scan.nextLine();
                exe = exe + answer + ", ";
                System.out.println("Do you want to add another column?");
                answer = scan.next();
            }
            System.out.println("The generated create statement is: \"" + exe + "\"");
            System.out.println("Do you want to execute?");
            exe = exe.substring(0, exe.length() - 2);
            exe = exe + ");";
            answer = scan.next();
            if (answer.charAt(0) == 'Y' || answer.charAt(0) == 'y') {
                s.execute(exe);
            }
            System.out.println("Do you want to create another Table?");
            answer = scan.next();
        }
        s.close();
        scan.close();
    }

    /**
     * Closes the driver to prevent memory loss. De Facto Destructor.
     */
    public void close() {
        try {
            conn.close();
            System.out.println("....Connection Successfully Closed....");
        } catch (SQLException e) {
            System.out.println("Connection was not closed, watch out for memory leaks");
        }
    }
}
