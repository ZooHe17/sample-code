/**
 * Zhuohong (Zooey) He
 * zhe@artskills.com
 * Smart Cabinet Project - January 10th, 2019
 *
 * This class controls the JavaFX UI. It handles all button clicks, implements
 * logic using multiple threads, and conveys notifications to users.
 */

package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

public class Controller {
    //Declaration Statements
    // Tunable Variables:
    private String scanPath;
    private String storePath;
    private String payScanPath;
    private String payStorePath;
    private final String[] pass = {"AS011419", "AS011419"}; //{Maintainence, Inquiry_edit}
    private final String[] inv_lookup = {"inv_num", "po_num", "inv_date", "cust"};
    private final String[] pay_lookup = {"chk_num", "amt_num", "dep_date", "cust"};

    sqliteDriver storage;
    ocrDriver ocr = new ocrDriver(3399, 4400);

    //--------JavaFX GUI Elements--------
    //INVOICE FILING TAB
    @FXML ListView<String> fileList;
    @FXML TextField invoiceTxfd, poTxfd;
    @FXML ComboBox<String> customerCmbx;
    @FXML DatePicker invoiceDtpkr;
    @FXML ImageView pdfImvw;
    @FXML Label invErrorLbl, invConfirmLbl;
    @FXML ProgressIndicator invPrgInd;

    @FXML Button doneBut, refreshBut, closeBut, invDeleteBut;

    //PAYMENT FILING TAB
    @FXML ListView<String> payFileList;
    @FXML TextField chkNumTxfd, amtTxfd;
    @FXML ComboBox<String> payCustCmbx;
    @FXML DatePicker depositDtpkr;
    @FXML ImageView payPdfImvw;
    @FXML Label payErrorLbl, payConfirmLbl;
    @FXML ScrollPane payScrollPane;
    @FXML ProgressIndicator payPrgInd;

    @FXML Button payDoneBut, payRefreshBut, payCloseBut, payDeleteBut;

    //INQUIRY TAB
    @FXML ChoiceBox<String> typeChbx;
    @FXML TextField oneTxfd, twoTxfd, threeMonTxfd, threeDayTxfd, threeYearTxfd, subMonTxfd, subDayTxfd, subYearTxfd;
    @FXML ComboBox<String> fourCmbx;
    @FXML Label oneLbl, twoLbl, threeLbl, fourLbl, inqErrorLbl;
    @FXML TableView<Entry> resultTbl;
    @FXML TableColumn oneCol, twoCol, threeCol, fourCol, fdateCol;
    @FXML PasswordField inqPwdFd;
    @FXML VBox inqVbox;

    @FXML Button searchBut, extractBut, editBut, deleteBut;

    //MAINTENANCE TAB
    @FXML TextField scanPathTxfd, cabPathTxfd, payScanPathTxfd, payCabPathTxfd;
    @FXML TextArea currCustTxar, allCustTxar;
    @FXML PasswordField pswdPwfd;
    @FXML Label manErrorLbl;

    @FXML Button saveBut, cancelBut;

    /**
     * Initializes the entire program
     */
    @FXML
    private void initialize() {
        System.out.println("Starting Up the Smart Cabinet");
        storage = new sqliteDriver();
        this.invoiceInitialize();
        this.paymentInitialize();
        this.inquiryInitialize();
        this.maintenanceInitialize();
        this.refreshAll();
    }

    //INITIALIZES   -   A set of functions that sets up each tab
    /**
     * Initialize invoice tab. Adds listener to automatically load image of
     * pdf when a file is selected in the list view.
     */
    private void invoiceInitialize() {
        invErrorLbl.setText(" ");
        fileList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {
                    invPrgInd.setVisible(true);
                    updatePDF();
                }
            }
        });
    }

    /**
     * Initialize payment tab. Adds listener to automatically load image of
     * pdf when a file is selected in the list view. Customizes scroll speed.
     */
    private void paymentInitialize() {
        payErrorLbl.setText(" ");
        payFileList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(newValue != null) {
                    payPrgInd.setVisible(true);
                    updatePayPDF();
                }
            }
        });

        //Increase Scroll Speed
        payPdfImvw.setOnScroll(new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                double deltaY = event.getDeltaY() * 3;
                double width = payScrollPane.getContent().getBoundsInLocal().getWidth();
                double vvalue = payScrollPane.getVvalue();
                payScrollPane.setVvalue(vvalue + -deltaY/width);
            }
        });
    }

    /**
     * Initialize inquiry tab. Sets up each column, enter key to search, sets
     * up label based on inquiry type.
     */
    private void inquiryInitialize(){
        inqErrorLbl.setText(" ");
        resultTbl.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        typeChbx.setItems(FXCollections.observableArrayList("Invoices", "Payments"));
        typeChbx.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String value, String new_value) {
                if (new_value.equals("Invoices")) {
                    oneLbl.setText("Invoice Number:");
                    twoLbl.setText("PO Number:");
                    threeLbl.setText("Invoice Date:");
                    fourLbl.setText("Customer:");
                    oneCol.setText("Invoice Number");
                    twoCol.setText("PO Number");
                    threeCol.setText("Invoice Date");
                    fourCol.setText("Customer");
                } else if (new_value.equals("Payments")) {
                    oneLbl.setText("Check Number:");
                    twoLbl.setText("Amount:");
                    threeLbl.setText("Deposit Date:");
                    fourLbl.setText("Customer:");
                    oneCol.setText("Check Number");
                    twoCol.setText("Amount");
                    threeCol.setText("Deposit Date");
                    fourCol.setText("Customer");
                }
                extractBut.setText("Extract " + new_value + " to Desktop");
                resultTbl.getItems().clear();
            }
        });

        typeChbx.getSelectionModel().selectFirst();

        oneCol.setCellValueFactory(new PropertyValueFactory<Entry, String>("one"));
        oneCol.setCellFactory(TextFieldTableCell.forTableColumn());
        oneCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Entry, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Entry, String> event) {
                        Entry change = ((Entry) event.getTableView().getItems().get(
                                event.getTablePosition().getRow()));

                        String newValue = event.getNewValue();
                        change.setOne(newValue);
                        update(change);
                        handleSearchBut(new ActionEvent());
                    }
                }
        );
        twoCol.setCellValueFactory(new PropertyValueFactory<Entry, String>("two"));
        twoCol.setCellFactory(TextFieldTableCell.forTableColumn());
        twoCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Entry, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Entry, String> event) {
                        Entry change = ((Entry) event.getTableView().getItems().get(
                                event.getTablePosition().getRow()));

                        String newValue = event.getNewValue();
                        change.setTwo(newValue);
                        update(change);
                        handleSearchBut(new ActionEvent());
                    }
                }
        );
        threeCol.setCellValueFactory(new PropertyValueFactory<Entry, String>("three"));
        threeCol.setCellFactory(TextFieldTableCell.forTableColumn());
        threeCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Entry, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Entry, String> event) {
                        Entry change = ((Entry) event.getTableView().getItems().get(
                                event.getTablePosition().getRow()));

                        String newValue = event.getNewValue();
                        change.setThree(newValue);
                        update(change);
                        handleSearchBut(new ActionEvent());
                    }
                }
        );
        fourCol.setCellValueFactory(new PropertyValueFactory<Entry, String>("four"));
        fourCol.setCellFactory(ComboBoxTableCell.forTableColumn(this.getCustomerObList(false)));
        fourCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Entry, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Entry, String> event) {
                        Entry change = ((Entry) event.getTableView().getItems().get(
                                event.getTablePosition().getRow()));

                        String newValue = event.getNewValue();
                        change.setFour(newValue);
                        update(change);
                        handleSearchBut(new ActionEvent());
                    }
                }
        );
        fdateCol.setCellValueFactory(new PropertyValueFactory<Entry, String>("subdate"));

        oneTxfd.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER) {
                    handleSearchBut(new ActionEvent());
                }
            }
        });

        twoTxfd.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER) {
                    handleSearchBut(new ActionEvent());
                }
            }
        });

        threeDayTxfd.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER) {
                    handleSearchBut(new ActionEvent());
                }
            }
        });

        threeMonTxfd.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER) {
                    handleSearchBut(new ActionEvent());
                }
            }
        });

        threeYearTxfd.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER) {
                    handleSearchBut(new ActionEvent());
                }
            }
        });

        fourCmbx.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER) {
                    handleSearchBut(new ActionEvent());
                }
            }
        });

        subDayTxfd.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER) {
                    handleSearchBut(new ActionEvent());
                }
            }
        });

        subMonTxfd.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER) {
                    handleSearchBut(new ActionEvent());
                }
            }
        });

        subYearTxfd.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if(event.getCode() == KeyCode.ENTER) {
                    handleSearchBut(new ActionEvent());
                }
            }
        });

        inqPwdFd.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    handleEditBut(new ActionEvent());
                }
            }
        });


    }

    /**
     * Initialize the maintenance tab. Currently empty.
     */
    private void maintenanceInitialize() {
        //FIXME Should I refresh this tab?
    }

    //INVOICE FILING BUTTON HANDLES
    /**
     * Checks invoices for bad/empty input, missing folders in folder
     * structure, stores files, and loads file info into SQL database.
     * @param event
     */
    @FXML
    private void handleDoneBut(ActionEvent event) {
        String file = fileList.getSelectionModel().getSelectedItem();
        if (file == null) {
            invErrorLbl.setText("Error: no file selected");
            return;
        } else if (file.charAt(file.length() - 1) == '~') {
            invErrorLbl.setText("Error: Wait for scanning to complete");
            return;
        } else if (invoiceTxfd.getText().equals("")) {
            invErrorLbl.setText("Error: no invoice number entered");
            return;
        } else if (poTxfd.getText().equals("")) {
            invErrorLbl.setText("Error: no PO number entered");
            return;
        } else if (customerCmbx.getValue() == null) {
            invErrorLbl.setText("Error: no customer selected");
            return;
        } else if (invoiceDtpkr.getValue() == null) {
            invErrorLbl.setText("Error: no date chosen");
            return;
        }
        String invn = invoiceTxfd.getText();
        String ponm = poTxfd.getText();
        String cust = customerCmbx.getValue();
        LocalDate ivdt = invoiceDtpkr.getValue();

        File oldFile = new File(scanPath + file);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy");

        //Make Folders if missing
        if (!checkFolder(storePath + ivdt.format(formatter) + "/")) {
            new File(storePath + ivdt.format(formatter) + "/").mkdir();
        }
        if (!checkFolder(storePath + ivdt.format(formatter) + "/" + cust + "/")) {
            new File(storePath + ivdt.format(formatter) + "/" + cust + "/").mkdir();
        }

        String filing = ivdt.format(formatter) + "/" + cust + "/";
        String pdf = invn + "_" +
                ponm + "_" +
                cust + "_" +
                ivdt.toString() + "_" +
                LocalDate.now().toString() +
                ".pdf";

        String filename = storePath + filing + pdf;
        File newFile = new File(filename);
        if (checkFile(newFile.getAbsolutePath())) {     //check for duplicates
            invErrorLbl.setText("Error: A file with this name already exists and was filed today. If you have made a filing error, contact your supervisor.");
            invConfirmLbl.setText(" ");
        } else if (storage.query("SELECT * FROM invoices WHERE inv_num = '" + invn + "' AND po_num = '" + ponm + "';").size() != 0) {
            invErrorLbl.setText("Error: This Invoice and PO number already exists. Please talk to your supervisor. Consider adding a version letter (ex. '-A')");
            invConfirmLbl.setText(" ");
        } else if (oldFile.length() == 0) {
            invErrorLbl.setText("Error: File size is 0, please try again.");
            invConfirmLbl.setText(" ");
        } else {
            oldFile.renameTo(newFile);
            storage.execute("INSERT INTO invoices (filename, address, filesize, inv_num, po_num, cust, inv_date, sub_date) " +
                    "VALUES ('" + pdf + "', '" + filename + "', '" + newFile.length() + "', '" + invn + "', '" + ponm +
                    "', '" + cust + "', '" + ivdt.toString() + "', '" + LocalDate.now().toString()+ "');");
            invConfirmLbl.setText("Stored: " + pdf);
            invErrorLbl.setText("");
            invoiceTxfd.setText("");
            poTxfd.setText("");
            customerCmbx.setValue(null);
            invoiceDtpkr.setValue(null);
        }
        this.refreshInvTab();
    }

    /**
     * Refreshes file list
     * @param event
     */
    @FXML
    private void handleRefreshBut(ActionEvent event) {
        refreshInvTab();
    }

    /**
     * Safely closes and exists the program
     * @param event
     */
    @FXML
    public void handleCloseBut(ActionEvent event) {
        Stage stage = (Stage) closeBut.getScene().getWindow();
        storage.close();
        stage.close();
    }

    /**
     * Deletes the file selected in the fileList
     * TODO: Make a pop-up window confirming decision to delete
     * @param event
     */
    @FXML
    public void handleInvDeleteBut(ActionEvent event) {
        String file = fileList.getSelectionModel().getSelectedItem();
        if (file == null) {
            return;
        }
        File deleteFile = new File(scanPath + file);
        if (!deleteFile.delete()) {
            System.err.println("Unable to delete file");
            invConfirmLbl.setText("");
            invErrorLbl.setText("Unable to delete file");
        } else {
            invConfirmLbl.setText("File: " + file + "  deleted");
            invErrorLbl.setText("");
        }
        this.refreshInvTab();
    }

    //Payment Filing BUTTON HANDLES
    /**
     * Checks payments for bad/empty input, missing folders in folder
     * structure, stores files, and loads file info into SQL database.
     * @param event
     */
    @FXML
    private void handlePayDoneBut(ActionEvent event) {
        String file = payFileList.getSelectionModel().getSelectedItem();
        if (file == null) {
            payErrorLbl.setText("Error: no file selected");
            return;
        } else if (file.charAt(file.length() - 1) == '~') {
            invErrorLbl.setText("Error: Wait for scanning to complete");
            return;
        } else if (chkNumTxfd.getText().equals("")) {
            payErrorLbl.setText("Error: no check number entered");
            return;
        } else if (amtTxfd.getText().equals("")) {
            payErrorLbl.setText("Error: no amount entered");
            return;
        } else if (payCustCmbx.getValue() == null) {
            payErrorLbl.setText("Error: no customer selected");
            return;
        } else if (depositDtpkr.getValue() == null) {
            payErrorLbl.setText("Error: no date chosen");
            return;
        }
        String chknum = chkNumTxfd.getText();
        String amt = amtTxfd.getText();
        String cust = payCustCmbx.getValue();
        LocalDate dpdt = depositDtpkr.getValue();

        File oldFile = new File(payScanPath + file);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy");

        //Make Folders if missing
        if (!checkFolder(payStorePath + "/" + dpdt.format(formatter) + "/")) {
            new File(payStorePath + "/" + dpdt.format(formatter) + "/").mkdir();
        }
        if (!checkFolder(payStorePath + "/" + dpdt.format(formatter) + "/" + cust + "/")) {
            new File(payStorePath + "/" + dpdt.format(formatter) + "/" + cust + "/").mkdir();
        }

        String filing = dpdt.format(formatter) + "/" + cust + "/";
        String pdf = chknum + "_" +
                amt + "_" +
                cust + "_" +
                dpdt.toString() + "_" +
                LocalDate.now().toString() +
                ".pdf";

        String filename = payStorePath + filing + pdf;
        File newFile = new File(filename);
        if (checkFile(newFile.getAbsolutePath())) {     //check for duplicates
            payErrorLbl.setText("Error: file with this name already exists");
            payConfirmLbl.setText(" ");
        } else if (storage.query("SELECT * FROM payments WHERE chk_num = '" + chknum + "';").size() != 0) {
            payErrorLbl.setText("Error: A file with this check number already exists. Please talk to you supervisor.");
            payConfirmLbl.setText(" ");
        } else if (oldFile.length() == 0) {
            payErrorLbl.setText("Error: File size is 0, please try again.");
            payConfirmLbl.setText(" ");
        } else {
            oldFile.renameTo(newFile);
            storage.execute("INSERT INTO payments (filename, address, filesize, chk_num, amt_num, cust, dep_date, sub_date) " +
                    "VALUES ('" + pdf + "', '" + filename + "', '" + newFile.length() + "', '" + chknum + "', '" + amt +
                    "', '" + cust + "', '" + dpdt.toString() + "', '" + LocalDate.now().toString()+ "');");
            payConfirmLbl.setText("Stored: " + pdf);
            payErrorLbl.setText("");
            chkNumTxfd.setText("");
            amtTxfd.setText("");
            payCustCmbx.setValue(null);
            depositDtpkr.setValue(null);
        }
        this.refreshPayTab();
    }

    /**
     * Refreshes Payment File List
     * @param event
     */
    @FXML
    private void handlePayRefreshBut(ActionEvent event) {
        this.refreshPayTab();
    }

    /**
     * Safely Closes the program from the payments tab
     * @param event
     */
    @FXML
    private void handlePayCloseBut(ActionEvent event) {
        handleCloseBut(new ActionEvent());
    }

    /**
     * Deletes the file selected in the payFileList
     * TODO: Make a pop-up window confirming decision to delete
     * @param event
     */
    @FXML
    private void handlePayDeleteBut(ActionEvent event) {
        String file = payFileList.getSelectionModel().getSelectedItem();
        if (file == null) {
            return;
        }
        File deleteFile = new File(payScanPath + file);
        if (!deleteFile.delete()) {
            System.err.println("Unable to delete file");
            payConfirmLbl.setText("");
            payErrorLbl.setText("Unable to delete file");
        } else {
            payConfirmLbl.setText("File: " + file + "  deleted");
            payErrorLbl.setText("");
        }
        this.refreshPayTab();
    }

    //Inquiry BUTTON HANDLES
    /**
     * Creates a database query statement based on data in the search boxes.
     * Allows for partial matches. Updates results into TableView
     * @param event
     */
    @FXML
    private void handleSearchBut(ActionEvent event){
        String type = typeChbx.getValue().toLowerCase();
        String select = "SELECT * FROM " + type + " WHERE ";
        boolean empty = true;
        if (!oneTxfd.getText().trim().isEmpty()) {
            empty = false;
            if (type.equals("invoices")) {
                select = select + inv_lookup[0];
            } else {
                select = select + pay_lookup[0];
            }
            select = select + " LIKE '%" + oneTxfd.getText().trim() + "%' AND ";
        }
        if (!twoTxfd.getText().trim().isEmpty()) {
            empty = false;
            if (type.equals("invoices")) {
                select = select + inv_lookup[1];
            } else {
                select = select + pay_lookup[1];
            }
            select = select + " LIKE '%" + twoTxfd.getText().trim() + "%' AND ";
        }

        if (!threeDayTxfd.getText().trim().isEmpty()) {
            empty = false;
            if (type.equals("invoices")) {
                select = select + "strftime('%d', " + inv_lookup[2] + ") = '";
            } else {
                select = select + "strftime('%d', " + pay_lookup[2] + ") = '";
            }
            select = select + threeDayTxfd.getText() + "' AND ";
        }

        if (!threeMonTxfd.getText().trim().isEmpty()) {
            empty = false;
            if (type.equals("invoices")) {
                select = select + "strftime('%m', " + inv_lookup[2] + ") = '";
            } else {
                select = select + "strftime('%m', " + pay_lookup[2] + ") = '";
            }
            select = select + threeMonTxfd.getText() + "' AND ";
        }

        if (!threeYearTxfd.getText().trim().isEmpty()) {
            empty = false;
            if (type.equals("invoices")) {
                select = select + "strftime('%Y', " + inv_lookup[2] + ") = '";
            } else {
                select = select + "strftime('%Y', " + pay_lookup[2] + ") = '";
            }
            select = select + threeYearTxfd.getText() + "' AND ";
        }

        if (!subDayTxfd.getText().trim().isEmpty()) {
            empty = false;
            select = select + "strftime('%d', sub_date) = '";
            select = select + subDayTxfd.getText() + "' AND ";
        }

        if (!subMonTxfd.getText().trim().isEmpty()) {
            empty = false;
            select = select + "strftime('%m', sub_date) = '";
            select = select + subMonTxfd.getText() + "' AND ";
        }

        if (!subYearTxfd.getText().trim().isEmpty()) {
            empty = false;
            select = select + "strftime('%Y', sub_date) = '";
            select = select + subYearTxfd.getText() + "' AND ";
        }

        if (!fourCmbx.getSelectionModel().isEmpty() && !fourCmbx.getSelectionModel().getSelectedItem().equals("")) {
            empty = false;
            if (type.equals("invoices")) {
                select = select + inv_lookup[3];
            } else {
                select = select + pay_lookup[3];
            }
            select = select + " = '" + fourCmbx.getSelectionModel().getSelectedItem().trim() + "' AND ";
        }

        ObservableList<Entry> e;
        if (!empty) {
            select = select.substring(0, select.length() - 5) + ";";
            e = storage.query(select);
        } else {
            e = storage.query("SELECT * FROM " + type);
        }

        resultTbl.setItems(e);
    }

    /**
     * Copies the file from location in filing system to user's desktop.
     * @param event
     */
    @FXML
    private void handleExtractBut(ActionEvent event) {
        ObservableList<Entry> selected = resultTbl.getSelectionModel().getSelectedItems();
        inqErrorLbl.setText("");
        if (selected.size() == 0) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            inqErrorLbl.setText("Nothing Selected");
            return;
        }
        for (int i = 0; i < selected.size(); i++) {
            File dest = new File(System.getProperty("user.home") + "/Desktop/" + selected.get(i).getFilename());
            File src = new File(selected.get(i).getAddress());
            try {
                FileUtils.copyFile(src, dest);
            } catch (IOException e) {
                System.err.println("The file was not found in the cabinet, this is bad news.");
                inqErrorLbl.setText("File was not found in the storage location. This is bad. Please contact the administrator.");
            }
        }
        inqErrorLbl.setTextFill(Color.web("#28784D"));
        inqErrorLbl.setText("Extracted all to desktop");
    }

    /**
     * Handles extract of tableview to csv
     * @param event
     */
    @FXML
    private void handleExtractCsvBut(ActionEvent event) {
        writeCsv(resultTbl, typeChbx.getValue().toLowerCase().charAt(0));
    }

    /**
     * Enters edit mode after matching passwords. Error statements if password
     * does not match.
     * @param event
     */
    @FXML
    private void handleEditBut(ActionEvent event) {
        if (!resultTbl.isEditable() && !pass[1].equals(inqPwdFd.getText())) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            inqErrorLbl.setText("Incorrect Password: Cannot go into edit mode");
        } else if (!resultTbl.isEditable()) {
            inqErrorLbl.setText("EIDT MODE");
            resultTbl.setEditable(true);
            editBut.setText("Stop Editing");
            inqVbox.setStyle("-fx-background-color: #A06050;");
        } else if (resultTbl.isEditable()) {
            resultTbl.setEditable(false);
            inqErrorLbl.setText(" ");
            inqPwdFd.setText("");
            editBut.setText("Edit Mode");
            inqVbox.setStyle("-fx-background-color: #90AFC5;");
        }
    }

    /**
     * Removes the file from the database and filing location. Requires correct
     * password.
     * @param event
     */
    @FXML
    private void handleDeleteBut(ActionEvent event){
        if (!pass[1].equals(inqPwdFd.getText())) {
            inqErrorLbl.setText("Incorrect Password: cannot delete");
        } else if (resultTbl.getSelectionModel().getSelectedItem() != null) {
            ObservableList<Entry> e = resultTbl.getSelectionModel().getSelectedItems();
            inqErrorLbl.setTextFill(Color.web("#28784D"));
            String msg = "File(s) Deleted: ";
            for (int i = 0; i < e.size(); i++) {
                File f = new File(e.get(i).getAddress());
                if (f.delete()) {
                    storage.execute("DELETE FROM " + typeChbx.getValue().toLowerCase() + " WHERE key = '" + e.get(i).getKey() + "';");
                    resultTbl.getItems().remove(e);
                    msg = msg + "\n\n" + e.get(i).getFilename();
                } else {
                    inqErrorLbl.setTextFill(Color.web("#bb0000"));
                    msg += "\n\nCannot Delete: " + e.get(i).getFilename();
                }
            }
            inqErrorLbl.setText(msg);
        }
    }

    //Maintenance BUTTON HANDLES
    /**
     * Saves directory and customer settings based on entered data.
     * Stores into the database.
     * @param event
     */
    @FXML
    private void handleSaveBut(ActionEvent event){
        boolean valid = true;
        String[] t = currCustTxar.getText().split("\n");
        for (int i = 0; i < t.length; i++) {
            if (!validFileName(t[i])) {
                valid = false;
            }
        }
        t = allCustTxar.getText().split("\n");
        for (int i = 0; i < t.length; i++) {
            if (!validFileName(t[i])) {
                valid = false;
            }
        }

        if (!pass[0].equals(pswdPwfd.getText())) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            manErrorLbl.setText("Wrong Password, Access Denied");
        } else if (!checkFolder(scanPathTxfd.getText())) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            manErrorLbl.setText("Error: Invoice Scanner Folder Address Invalid");
        } else if (!checkFolder(cabPathTxfd.getText())) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            manErrorLbl.setText("Error: Invoice Cabinet Folder Address Invalid");
        } else if (!checkFolder(payScanPathTxfd.getText())){
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            manErrorLbl.setText("Error: Payment Scanner Folder Address Invalid");
        } else if (!checkFolder(payCabPathTxfd.getText())) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            manErrorLbl.setText("Error: Payment Cabinet Folder Address Invalid");
        } else if (!valid) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            manErrorLbl.setText("Error: Customer name contains invalid characters");
        } else {
            storage.execute("DELETE FROM settings;");
            storage.execute("INSERT INTO settings (setting, value) VALUES ('inv_scan', '" + scanPathTxfd.getText() + "');");
            storage.execute("INSERT INTO settings (setting, value) VALUES ('inv_file', '" + cabPathTxfd.getText() + "');");
            storage.execute("INSERT INTO settings (setting, value) VALUES ('pay_scan', '" + payScanPathTxfd.getText() + "');");
            storage.execute("INSERT INTO settings (setting, value) VALUES ('pay_file', '" + payCabPathTxfd.getText() + "');");

            for (int i = 0; i < t.length; i++) {
                storage.execute("INSERT INTO settings (setting, value) VALUES ('act_cust', '" + t[i] + "');");
            }

            t = allCustTxar.getText().split("\n");
            for (int i = 0; i < t.length; i++) {
                storage.execute("INSERT INTO settings (setting, value) VALUES ('cust', '" + t[i] + "');");
            }
            this.refreshAll();
            inqErrorLbl.setTextFill(Color.web("#28784D"));
            manErrorLbl.setText("Successfully Saved");
        }
    }

    /**
     * Refreshes the maintenance page to revert all textboxes to previously
     * saved state.
     * @param event
     */
    @FXML
    private void handleCancelBut(ActionEvent event) {
        refreshManTab();
    }

    //REFRESHES
    /**
     * Reload scanned unsaved invoices list. Refreshes the customer combobox
     */
    private void refreshInvTab() {
        //Refresh the File list
        invoiceTxfd.setText("");
        poTxfd.setText("");
        invoiceDtpkr.setValue(null);
        customerCmbx.setValue(null);
        File[] scanFiles = new File(scanPath).listFiles();
        ObservableList<String> scanFileList = FXCollections.observableArrayList();
        for (File f:scanFiles) {
            scanFileList.add(f.getName());
        }

        fileList.setItems(scanFileList);

        //Refresh the customer list
        ObservableList<String> customerList = this.getCustomerObList(false);
        customerCmbx.setItems(customerList);


        if (fileList.getItems().size() != 0) {
            fileList.getSelectionModel().select(0);
        } else {
            pdfImvw.setImage(null);
        }
    }

    /**
     * Reloads the unsaved payments list. Refreshes the customer combobox
     */
    private void refreshPayTab() {
        chkNumTxfd.setText("");
        amtTxfd.setText("");
        depositDtpkr.setValue(null);
        payCustCmbx.setValue(null);
        File[] scanFiles = new File(payScanPath).listFiles();
        ObservableList<String> scanFileList = FXCollections.observableArrayList();
        for (File f:scanFiles) {
            scanFileList.add(f.getName());
        }

        payFileList.setItems(scanFileList);

        //Refresh the customer list
        ObservableList<String> customerList = this.getCustomerObList(false);
        payCustCmbx.setItems(customerList);

        if (payFileList.getItems().size() != 0) {
            payFileList.getSelectionModel().select(0);
        } else {
            payPdfImvw.setImage(null);
        }
    }

    /**
     * Reloads the items in the inquiry tab.
     */
    private void refreshInqTab() {
        fourCmbx.setItems(this.getCustomerObList(true));
    }

    /**
     * Reload the values in the maintenance tab from database.
     */
    private void refreshManTab() {
        ArrayList<String[]> arr = storage.queryArrList("SELECT * FROM settings WHERE setting = 'inv_scan';");
        scanPath = arr.get(0)[2];
        scanPathTxfd.setText(scanPath);
        arr = storage.queryArrList("SELECT * FROM settings WHERE setting = 'inv_file';");
        storePath = arr.get(0)[2];
        cabPathTxfd.setText(storePath);
        arr = storage.queryArrList("SELECT * FROM settings WHERE setting = 'pay_scan';");
        payScanPath = arr.get(0)[2];
        payScanPathTxfd.setText(payScanPath);
        arr = storage.queryArrList("SELECT * FROM settings WHERE setting = 'pay_file';");
        payStorePath = arr.get(0)[2];
        payCabPathTxfd.setText(payStorePath);

        String areaText = "";
        arr = storage.queryArrList("SELECT * FROM settings WHERE setting = 'act_cust';");
        for (int i = 0; i < arr.size(); i++) {
            areaText = areaText + arr.get(i)[2] + "\n";
        }
        currCustTxar.setText(areaText);

        arr = storage.queryArrList("SELECT * FROM settings WHERE setting = 'cust';");
        areaText = "";
        for (int i = 0; i < arr.size(); i++) {
            areaText = areaText + arr.get(i)[2] + "\n";
        }
        allCustTxar.setText(areaText);
        pswdPwfd.setText("");
    }

    /**
     * Runs all refreshes
     */
    private void refreshAll() {
        this.refreshManTab();
        this.refreshInvTab();
        this.refreshPayTab();
        this.refreshInqTab();
    }

    //Helper Methods

    /**
     * Converts first page of PDF into image. Loads image into invoice tab.
     * Uses OCR to read the image for data, which is then written to textboxes.
     */
    private void updatePDF() {
        new Thread(() -> {
            Platform.runLater(()-> invPrgInd.setProgress(0.0));
            try (final PDDocument document = PDDocument.load(new File(scanPath + fileList.getSelectionModel().getSelectedItem()))){
                PDFRenderer pdfRenderer = new PDFRenderer(document);
                Platform.runLater(() -> invPrgInd.setProgress(0.14));
                BufferedImage bim = pdfRenderer.renderImageWithDPI(0, 400, ImageType.RGB);
                Platform.runLater(() -> invPrgInd.setProgress(0.29));
                bim = bim.getSubimage(0,0, bim.getWidth(), bim.getHeight() * 4 / 7);
                Platform.runLater(() -> invPrgInd.setProgress(0.43));
                pdfImvw.setImage(SwingFXUtils.toFXImage(bim, null));
                Platform.runLater(()-> invPrgInd.setProgress(0.57));
                String[] results = ocr.readInvoice(bim);
                Platform.runLater(() -> invPrgInd.setProgress(0.71));
                Platform.runLater(() -> invSetValues(results));
                Platform.runLater(() -> invPrgInd.setProgress(0.86));
            } catch (IOException e){
                System.err.println("Exception while trying to create pdf document - " + e);
            }
            invPrgInd.setProgress(1.0);
            invPrgInd.setVisible(false);
        }).start();
    }

    /**
     * Convert first page of PDF into image. Loads image into invoice tab.
     */
    private void updatePayPDF() {
        new Thread(() -> {
            Platform.runLater(()-> payPrgInd.setProgress(0.0));
            try (final PDDocument document = PDDocument.load(new File(payScanPath + payFileList.getSelectionModel().getSelectedItem()))){
                Platform.runLater(()-> payPrgInd.setProgress(0.25));
                PDFRenderer pdfRenderer = new PDFRenderer(document);
                Platform.runLater(()-> payPrgInd.setProgress(0.50));
                System.out.println("RUN");
                BufferedImage bim = pdfRenderer.renderImageWithDPI(0, 400, ImageType.RGB);
                System.out.println("RUN");
                Platform.runLater(()-> {
                    payPrgInd.setProgress(0.75);
                    System.out.println("RUN");
                    payPdfImvw.setImage(SwingFXUtils.toFXImage(bim, null));
                });
            } catch (IOException e){
                System.err.println("Exception while trying to create pdf document - " + e);
            }
            Platform.runLater(()-> {
                payPrgInd.setProgress(1.0);
                payPrgInd.setVisible(false);
            });
        }).start();
    }

    /**
     * Sets the invoice tab textbox values
     * @param results String array with intended textbox values
     */
    public void invSetValues(String[] results) {
        invoiceTxfd.setText(results[0]);
        poTxfd.setText(results[1]);

        String date = results[3];
        LocalDate d;
        String regDate;
        try {
            regDate = formatDate(date);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            d = LocalDate.parse(regDate, formatter);
        } catch (Exception e) {
            System.out.println("unable to format date");
            return;
        }

        invoiceDtpkr.setValue(d);

        System.out.println("------------------------");
        System.out.println(results[0]);
        System.out.println(results[1]);
        System.out.println(results[2]);
        System.out.println(regDate);
    }

    /**
     * Updates one square in the SQL database, changes the location of the file
     * in the directory
     * @param e entry
     */
    private void update(Entry e) {
        String filing;
        String file;
        String statement = "UPDATE " + e.getType() + " SET ";

        if (e.getType().equals("payments")) {
            filing = payStorePath;
            statement = statement +
                    "chk_num = '" + e.getOne() +
                    "', amt_num = '" + e.getTwo() +
                    "', cust = '" + e.getFour() +
                    "', dep_date = '" + e.getThree() + "', ";
        } else {
            filing = storePath;
            statement = statement +
                    "inv_num = '" + e.getOne() +
                    "', po_num = '" + e.getTwo() +
                    "', cust = '" + e.getFour() +
                    "', inv_date = '" + e.getThree() + "', ";
        }


        file = e.getOne() + "_" +
                e.getTwo() + "_" +
                e.getFour() + "_" +
                e.getThree() + "_" +
                e.getSubdate() +
                ".pdf";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy");
        int[] intDate = parseDate(e.getThree());
        LocalDate ThreeDt = LocalDate.of(intDate[2], intDate[1], intDate[0]);
        if (!checkFolder(filing + ThreeDt.format(formatter) + "/")) {
            new File(filing + ThreeDt.format(formatter) + "/").mkdir();
        }
        if (!checkFolder(filing + ThreeDt.format(formatter) + "/" + e.getFour() + "/")) {
            new File(filing + ThreeDt.format(formatter) + "/" + e.getFour() + "/").mkdir();
        }
        filing = filing + ThreeDt.format(formatter) + "/" + e.getFour() + "/" + file;
        File oldFile = new File(e.getAddress());
        File newFile = new File(filing);
        if (checkFile(newFile.getAbsolutePath())) {     //check for duplicates
            inqErrorLbl.setText("Error: A file with this name already exists");
            invConfirmLbl.setText(" ");
        } else {
            oldFile.renameTo(newFile);
            statement = statement + " filename = '" + file + "', address = '" + filing + "' WHERE key = '" + e.getKey() + "';";
            System.out.println("change statement = " + statement);
            storage.execute(statement);
            inqErrorLbl.setText("Changed to: " + file);
        }

    }

    /**
     * Returns the customers in an observable list of strings.
     * @param all if true, returns all customers, if false returns only current customers
     * @return observable list of customer strings
     */
    private ObservableList<String> getCustomerObList(boolean all) {
        ArrayList<String[]> arr;
        if (all) {
            arr = storage.queryArrList("SELECT * FROM settings WHERE setting = 'cust';");
        } else {
            arr = storage.queryArrList("SELECT * FROM settings WHERE setting = 'act_cust';");
        }

        ObservableList<String> customerList = FXCollections.observableArrayList();
        customerList.add("");
        for (int i = 0; i < arr.size(); i++) {
            customerList.add(arr.get(i)[2]);
        }

        return customerList;
    }

    //Static Helper Methods
    /**
     * Returns the line number, used when printing errors and traces on
     * System.out and System.err
     * @return string line number
     */
    public static int lineNumber() {
        return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }


    /**
     * Writes the tableView into a csv file. Saves onto desktop
     * @param tb the tableview we would like to extract
     * @param type either 'i' or 'p' for invoice/payment
     */
    public void writeCsv(TableView<Entry> tb, char type) {
        ObservableList<Entry> e = tb.getItems();
        int numCol = tb.getColumns().size();
        String csv = "";
        for (int j = 0; j < numCol; j++) {
            csv = csv + "=\"" + tb.getColumns().get(j).getText() + "\",";
        }
        csv = csv.substring(0, csv.length() - 1) + "\n";
        for (int i = 0; i < e.size(); i ++) {
            for (int k = 0; k < numCol; k++) {
                csv = csv + "=\"" + tb.getColumns().get(k).getCellData(i).toString() + "\",";
            }
            csv = csv.substring(0, csv.length() - 1) + "\n";
        }

        DirectoryChooser fileChooser = new DirectoryChooser();
        File csvLoc = fileChooser.showDialog(extractBut.getScene().getWindow());
        if (csvLoc == null) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            inqErrorLbl.setText("No file chosen");
            return;
        }

        if (!this.checkFolder(csvLoc.toString())) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            inqErrorLbl.setText("Please select a folder");
            return;
        }

        try {
            PrintWriter out = new PrintWriter(csvLoc.toString() + "\\SmartCab_Table_Export.csv");
            out.println(csv);
            out.close();
            inqErrorLbl.setTextFill(Color.web("#28784D"));
            inqErrorLbl.setText("Extracted as SmartCab_Table_Export.csv");
        } catch (FileNotFoundException ex) {
            inqErrorLbl.setTextFill(Color.web("#bb0000"));
            inqErrorLbl.setText("Folder not found, or csv file currently being edited by other source.");
            return;
        }

    }

    /**
     * Checks to see if a file with the same name already escapes
     * @param path to the file.
     * @return true if file already exists
     */
    public static boolean checkFile(String path) {
        File f = new File(path);
        return f.exists() && f.isFile();
    }

    /**
     * Checks to see if folder with the same name already exists
     * @param path to the folder
     * @return true if the folder already exists
     */
    public static boolean checkFolder(String path) {
        File f = new File(path);
        return f.exists() && f.isDirectory();
    }

    /**
     * Checks a string to see if there are invalid file characters
     * @param s input string
     * @return true if valid filename
     */
    public static boolean validFileName(String s) {
        if (s.contains("#") || s.contains("'") || s.contains("/") || s.contains("\\") || s.contains("\"") || s.contains(":")) {
            return false;
        }
        return true;
    }

    /**
     * Formats dates like 1/1/2019 to 01/01/2019
     * @param date original string
     * @return reformatted date
     */
    public static String formatDate(String date) {
        String regDate = "";
        int len = 0;
        int k = 0;
        for (int i = 0; i < date.length(); i++) {
            if (date.charAt(i) >= '0' && date.charAt(i) <= '9') {
                len++;
            } else if (date.charAt(i) == '/') {
                if (len < 2) {
                    regDate = regDate.substring(0, k-1) + "0" + regDate.charAt(k-1);
                    k++;
                }

                len = 0;
            }
            regDate = regDate + date.charAt(i);
            k++;
        }
        return regDate;
    }

    /**
     * Returns integer array = {Year, Month, Day}
     * @param date string date with format MM/DD/YYYY
     * @return integer array with date information.
     */
    public static int[] parseDate(String date) {
        //date = formatDate(date);
        System.out.println(date);
        int[] results = new int[3];
        results[2] = Integer.parseInt(date.substring(0, 4));
        results[1] = Integer.parseInt(date.substring(5, 7));
        results[0] = Integer.parseInt(date.substring(8));
        return results;
    }
}