/**
 * Zhuohong (Zooey) He
 * zhe@artskills.com
 * Smart Cabinet Project - January 10th, 2019
 *
 * The Smart Cabinet Project was written for ArtSkills Inc a producer of mass
 * market consumer packaged goods (CPGs). The program organizes files generated
 * by the company's operation's department. The files from various sources are
 * given an appropriate name, moved into a logical folder structure, and
 * registered in a SQLite database. The program's search feature allows users
 * see and retrieve stored files and edit stored file information.
 */
package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("sample.fxml"));
        Parent root = loader.load();
        Controller control = loader.getController();
        primaryStage.setTitle("Smart Cabinet");
        primaryStage.getIcons().add(new Image("Cabinet-icon.png"));
        Scene scene = new Scene(root, 1000, 686);
        scene.getStylesheets().add("style.css");
        primaryStage.setScene(scene);
        primaryStage.setOnHidden(e -> {control.handleCloseBut(new ActionEvent());});
        primaryStage.show();
    }

    /**
     * Starts the application
     * @param args command line arguments (unused)
     */
    public static void main(String[] args) {
        launch(args);
    }

}
