## Smart Cabinet Filing Application  
By Zhuohong He  
zhe@artskills.com  
zhe17@jhu.edu  
January 21, 2019  

**Project Description:** The Smart Cabinet Project was written for ArtSkills Inc a producer of mass
market consumer packaged goods (CPGs). The program organizes files generated
by the company's operation's department. The files from various sources are
given an appropriate name, moved into a logical folder structure, and
registered in a SQLite database. The program's search feature allows users
see and retrieve stored files and edit stored file information.  

**Setup Instructions:** Before running, make sure to place the english OCR package "eng.traineddata" into the
java path at the following location "/{User Home}/Program Files (x86)/Common Files/Oracle/java/javapath/tessdata/".  

To run, either maven package the file and run the jar
file under the target folder, or maven compile and run the main method in
Main.java.

**Operation Instructions:** Use the first two tabs "invoice" and "payment" to file the relative documents. Use the
inquiry tab look up and extract stored documents. Use the maintenance tab to change the storage directory and customer
lists.