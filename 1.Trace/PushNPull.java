package com.zhuohong.he.volvo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

public class PushNPull {

    //Create method to make "data" variable from cloud data.
    //data contains all the repairs pertaining to a specific vehicle.

    //Tunable Values
    private String CSVAddress = "./src/sample/SampleRepairData.CSV";
    private final int stallNum = 5;
    private String connectionUrl = "jdbc:sqlserver://repairdata.database.windows.net:1433;database=Repair Data;" +
            "user=zhuohong.he@repairdata;password=1NN0v@tion;encrypt=true;" +
            "trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
    private Connection con = null;
    private Statement statement = null;
    private ResultSet rs = null;
    private String plantLoc = "LVO";
    private int numStalls = 38;

    /**
     * This method all repairs relating to the model and chassis number
     * @param model Model number of the truck in query
     * @param chassis Chassis number of the truck in query
     * @param table the table to query in
     * @return an observable list with the search results
     */
    public ObservableList<Repair> findAll(String model, String chassis, String table) {
        String sqlStatement = "SELECT * FROM " + table + " " +
                "WHERE MN = '" + model + "' AND CN = '" + chassis + "';";
        return querySQL(sqlStatement);
    }

    /**
     * This method gets the incomplete jobs relating to a specific truck from the Azure SQL database.
     * @param model Model number of the truck in question
     * @param chassis chassis number of the truck in question
     * @param table the table to look in
     * @return an observable list with the search results
     */
    public ObservableList<Repair> findIncomplete(String model, String chassis, String table) {
        System.out.println ("56: + " + model + "  " + chassis);
        String sqlStatement = "SELECT * FROM " + table + " " +
                "WHERE MN = '" + model + "' AND CN = '" + chassis + "' AND RS IS NULL;";
        return querySQL(sqlStatement);
    }

    /**
     * Delete a list of items from a specific table.
     * @param marked the list of selected items to delete
     * @param table the name of the SQL table to delete from
     * @return the number of deleted items.
     */
    public int deleteRepairs(ObservableList<Repair> marked, String table) {
        int numDelete = 0;
        for (Repair r : marked) {
            numDelete += updateSQL("DELETE FROM " + table + " WHERE uID= '" + r.getUID() + "';");
        }
        return numDelete;
    }

    /**
     * Updates the SQL Database with completed tasks
     * @param done an observable list with the repairs that are marked as finished
     * @return the number of items marked
     */
    public int completeTasks(ObservableList<Repair> done, Timestamp start, String sig, String table){
        int numDone = 0;
        for (Repair r : done) {
            String updateStr = "UPDATE " + table +
                    " SET RS= ?, RE= ?, RB= ?" +
                    " WHERE uID= ? ;";

            try {
                PreparedStatement prep = con.prepareStatement(updateStr);
                prep.setTimestamp(1, start);
                prep.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
                prep.setString(3, sig);
                prep.setString(4, r.getUID());

                numDone = updateSQL(prep);
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Error updating completed tasks");
            }
        }
        return numDone;
    }

    /**
     * Runs a specified query on the SQL database
     * @param inputStatement the SQL command that will be executed
     * @return an ObservableList with the result repairs
     */
    public ObservableList<Repair> querySQL (String inputStatement) {
        ObservableList<Repair> ret = FXCollections.observableArrayList();
        try {
            statement = con.createStatement();
            rs = statement.executeQuery(inputStatement);
            while(rs.next()){
                String[] params = {rs.getString("MN"), rs.getString("CN"), rs.getString("RC"),
                        rs.getString("RR"), rs.getString("ST"), rs.getString("SB"),
                        rs.getString("PL"), rs.getString("MP"), rs.getString("PPE"),
                        rs.getString("RS"), rs.getString("RE"), rs.getString("RB"),
                        rs.getString("uID"), rs.getString("gID"), rs.getString("Fixed")};
                ret.add(new Repair(params));
            }
        } catch (SQLException e) {
            System.err.println("Error querying statement: " + inputStatement);
        }
        return ret;
    }

    /**
     * This method is used to update/insert data within the SQL database.
     * @param prep input statement used to run the SQL Query
     * @return how many entries were updated/created
     */
    public int updateSQL (PreparedStatement prep) {
        int numChanged = 0;

        try {
            //statement = con.createStatement();
            numChanged = prep.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error updating statement: " + prep.toString());
            e.printStackTrace();
        }

        return numChanged;
    }

    /**
     * Overloaded method update SQL that takes a SQL string as opposed to a prepared statement.
     * Executes the INSERT and DELETE operations.
     * @param updateStr SQL command used to modify table
     * @return the number of modified entries
     */
    public int updateSQL (String updateStr) {
        int affected = 0;
        try {
            statement = con.createStatement();
            affected = statement.executeUpdate(updateStr);
        } catch (SQLException e) {
            System.err.println("Error updating statement: " + updateStr);
        }
        return affected;

    }

    /**
     * This method updates the stall status table with new information
     * @param mc the model chassis number of the vehicle in the stall
     * @param in the timestamp of when the truck entered the stall
     * @param filled stall occupation stored as (E)mpty or (F)ull
     * @param stat stall assistance status stored as (N)ormal or (H)elp
     * @return
     */
    public int updateStallStatus(String mc, LocalDateTime in, char filled, char stat) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE STALL_STATUS ");
        sb.append("SET ModelChassis=? InTime=? filled=? stat=? ");
        sb.append("WHERE StallNum='?' AND loc='?';");
        try {
            //Assignments
            PreparedStatement st = con.prepareStatement(sb.toString());
            st.setString(1, mc);
            st.setTimestamp(2, Timestamp.valueOf(in));
            st.setString(3, filled+"");
            st.setString(4, stat+"");
            //Where
            st.setInt(5,stallNum);
            st.setString(6, plantLoc);
        } catch (SQLException e) {
            System.err.print("Unable to update stall status");
            e.printStackTrace();
        }

        return 1;
    }

    public int updateStallStatus(StageWithData currWindow) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE STALL_STATUS ");
        sb.append("SET ModelChassis=?, InTime=?, filled=?, stat=? ");
        sb.append("WHERE StallNum=? AND loc=?;");
        try {
            //Assignments
            PreparedStatement st = con.prepareStatement(sb.toString());
            System.out.println("Setting: " + currWindow.getModelChassis() + ", " + currWindow.getStartTime());
            st.setString(1, currWindow.getModelChassis());
            st.setTimestamp(2, Timestamp.valueOf(currWindow.getStartTime()));
            st.setString(3, currWindow.getFilled());
            st.setString(4, currWindow.getStatus());
            //Where
            st.setInt(5,stallNum);
            st.setString(6, plantLoc);
            updateSQL(st);
            System.out.println("Done updating ");
        } catch (SQLException e) {
            System.err.println("Unable to update stall status");
            e.printStackTrace();
        }

        return 1;
    }

    /**
     * Constructor for the PushNPull class. Establishes connection
     */
    public PushNPull() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            System.out.println("Error finding server");
            e.printStackTrace();
        }

        try {
            con = DriverManager.getConnection(connectionUrl);
            // Define the SQL string.
            String sqlString = "";

            // Use the connection to create the SQL statement.
            statement = con.createStatement();

            // Execute the statement.
            statement.executeUpdate(sqlString);

            // Provide a message when processing is complete.
            System.out.println("Connected to Database");
        } catch (SQLException e) {
            System.out.println("Error establishing connection");
            e.printStackTrace();
        }
    }

    /**
     * populates the sql table with 100 random entries.
     */
    public void populateSQL() {
        for (int x = 1; x < 20; x++) {
            String modelNum = "";
            String chassisNum = "";
            for (int y = 0; y < 2; y++) { modelNum += (new Random()).nextInt(2); }
            for (int z = 0; z < 6; z++) { chassisNum += (new Random()).nextInt(2);}

            modelNum = "00";
            chassisNum = "110101";

            int cat = (new Random()).nextInt(10);
            String egory = "";

            switch (cat) {
                case 0: egory = "Hood"; break;
                case 1: egory = "Cab"; break;
                case 2: egory = "Sleeper Box"; break;
                case 4: egory = "Engine"; break;
                case 5: egory = "Transmission"; break;
                case 6: egory = "Axle/Chassis Front"; break;
                case 7: egory = "Chassis Center"; break;
                case 8: egory = "Chassis Rear"; break;
                case 9: egory = "Electrical"; break;
                default: egory = "Pnematics";
            }



            String signedBy1 = "";
            String signedBy2 = "";
            for (int z = 0; z < 6; z++) {
                signedBy1 += (new Random()).nextInt(8);
                signedBy2 += (new Random()).nextInt(8);
            }

            int datee = (new Random()).nextInt(30);

            try {
                int num = (new Random()).nextInt(80);
                int num2 = (new Random()).nextInt(33) - 3;

                if (num >= 30 && num2 > 0) {
                    StringBuilder sb = new StringBuilder();

                    sb.append("INSERT INTO RepairTable");
                    sb.append("(MN, CN, RC, RR, ST, SB, PL, MP, PPE, RS, RE, RB) ");
                    sb.append("VALUES ( ");
                    sb.append("?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?");
                    sb.append(");");
                    PreparedStatement st = con.prepareStatement(sb.toString());
                    st.setString(1, modelNum);
                    st.setString(2, chassisNum);
                    st.setString(3, egory);
                    st.setString(4, "Very detailed notes on repair specifications and instructions on procedure.");
                    st.setTimestamp(5, (new Timestamp(System.currentTimeMillis())));
                    st.setString(6, signedBy1);
                    st.setString(7, "Sample Lot");
                    st.setString(8, "Missing parts go here");
                    GregorianCalendar g = new GregorianCalendar(2018, 5, datee);
                    st.setDate(9, new java.sql.Date(g.getTimeInMillis()));
                    st.setTimestamp(10, new Timestamp(System.currentTimeMillis() - num2 * 86436000));
                    st.setTimestamp(11, new Timestamp(System.currentTimeMillis() - num2 * 86436000 + num * 60000));
                    st.setString(12, signedBy2);
                    updateSQL(st);
                } else {
                    StringBuilder sb = new StringBuilder();

                    sb.append("INSERT INTO RepairTable");
                    sb.append("(MN, CN, RC, RR, ST, SB, PL, MP, PPE) ");
                    sb.append("VALUES ( ");
                    sb.append("?, ?, ?, ?, ?, ?, ?, ?, ?");
                    sb.append(");");
                    PreparedStatement st = con.prepareStatement(sb.toString());
                    st.setString(1, modelNum);
                    st.setString(2, chassisNum);
                    st.setString(3, egory);
                    st.setString(4, "Very detailed notes on repair specifications and instructions on procedure.");
                    st.setTimestamp(5, (new Timestamp(System.currentTimeMillis())));
                    st.setString(6, signedBy1);
                    st.setString(7, "Sample Lot");
                    st.setString(8, "Missing parts go here");
                    GregorianCalendar g = new GregorianCalendar(2018, 5, datee);
                    st.setDate(9, new java.sql.Date(g.getTimeInMillis()));
                    updateSQL(st);
                }


            } catch (SQLException e) {
                System.out.println("Error forming Prepared Statement");
            }
        }
    }

    /**
     * Initializes the stall status table. Please set the global variables at the top of the class for each production
     * plant. Current settings are for Mack Trucks in Macungie, PA.
     */
    public void initializeStatusTable() {
        for (int x = 1; x <= numStalls; x++){
            StringBuilder sb = new StringBuilder();
            sb.append("INSERT INTO STALL_STATUS ");
            sb.append("(StallNum, ModelChassis, InTime, Stat, loc, filled) ");
            sb.append("VALUES (?, ?, ?, ?, ?, ?);");
            try {
                PreparedStatement st = con.prepareStatement(sb.toString());
                st.setInt(1, x);
                st.setString(2, StageWithData.defaultMC);
                st.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
                st.setString(4, "N");
                st.setString(5, plantLoc);
                st.setString(6, "E");
                updateSQL(st);
            } catch (SQLException e) {
                System.err.println("unable to populate status table");
                e.printStackTrace();
            }
        }

    }

    /**
     * Class destructor
     * @return whether the destructor was successfully closed.
     */
    public boolean close() {
        try
        {
            // Close resources.
            if (null != con) con.close();
            if (null != statement) statement.close();
            if (null != rs) rs.close();
        }
        catch (SQLException sqlException)
        {
            // No additional action if close() statements fail.
            return false;
        }

        System.out.println("Disconnected from Database");
        return true;
    }
}
