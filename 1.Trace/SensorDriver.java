package com.zhuohong.he.volvo;
import com.pi4j.io.i2c.I2CFactory;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.IOException;
import java.sql.SQLOutput;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Random;

public class SensorDriver implements abstractSensorDriver {

    private StageWithData currWindow;   //where we will be setting the model-chassis number
    private boolean truckPresent;       //Boolean for when a truck is present (related to sensorOn, but delayed by lag time)
    private boolean jobDone;            //notifies a truck completes the job
    private boolean sensorOn;           //

    //Adjustable (tunable) Parameters
    private int lagOn = 5;         //[sec] Change this to change how long the sensor needs to be covered before the process starts
    private int lagOff = 5;         //[sec] How long the sensor needs to be off for the process to end
    private int sensorInterval = 1; //[sec] How long between sensor scans
    private int inDistance = 200;   //[cm] Distance of truck in stall

    //Other Variables
    private int onTime;               //Amount of time of sensor in on state
    private int offTime;              //Amount of time of sensor in off state

    private LidarLiteSensorSerial lidar;
    private CameraSensor cam;

    /**
     * Default Constructor
     */
    public SensorDriver() {
        System.out.println("Error in SensorDriver.java: Missing parameter StageWithData");
    }


    /**
     * Constructor for SensorDriver
     *
     * @param window the current window, will be used to modify model-chassis number
     */
    public SensorDriver(StageWithData window) {
        currWindow = window;
        truckPresent = false;
        jobDone = false;
        sensorOn = false;
        onTime = 0;
        offTime = 0;

        try {
            lidar = new LidarLiteSensorSerial(currWindow);
            cam = new CameraSensor(window);

        } catch (RuntimeException g) {
            System.err.println("Error we cannot find the Lidar Sensor. Check to make sure system supports I2C communication");
        }
        //for dummy methods/tests
        testVariable = truckPresent;
    }

    @Override
    public void checkSensor() {

        int distance = -1;
        int reads = 0;

        while (distance == -1) {
            reads++;
            distance = Integer.parseInt(currWindow.getDebugText());

            if (reads > 6) {
                System.out.println("Error... unable to read sensor data.");
                break;
            }

        }


        if (distance <= inDistance && distance != -1) {
            sensorOn = true;
        } else {
            sensorOn = false;
        }

    }

    /**
     * updates the time in the StageWithData
     */
    @Override
    public void updateTime() {
        Platform.runLater(() -> {
            currWindow.setCurrTime();
        });
    }

    /**
     * Updates ModelChassis number by grabbing camera input
     */
    @Override
    public void updateModelChassis() {

        String[] parsedString;
        try {
            parsedString = cam.scanWindshield().split(" ");
            if (this.validateMC(parsedString) == false) {
                System.out.println("Error... the Barcode data is not formatted correctly");
                return;
            }

            Platform.runLater(() -> {
                currWindow.setModel(parsedString[0]);
                currWindow.setChassis(parsedString[1]);
                currWindow.setModelChassis("M-" + parsedString[0] + "-" + parsedString[1]);
            });

        } catch (QrNotFoundException e) {
            System.out.println("The QR code could not be found, initiating manual entry");

            Platform.runLater(() -> {
                currWindow.getHpController().startBut.fire();
            });

        }
    }

    /**
     * returns model chassis display to nothing
     */
    @Override
    public void nullModelChassis() {
        Platform.runLater(() -> {
            currWindow.setModelChassis(currWindow.getDefaultMC());
            currWindow.setModel("");
            currWindow.setChassis("");
        });
    }

    /**
     * A method to check whether truck is in the stall
     * updates global variable "sensorOn"
     */
    public boolean testVariable = false;

    /**
     * Method that manages sensor trips and starts camera action
     */
    @Override
    public void startCollection() {
        while (currWindow.isShowing()) {
            Platform.runLater(() -> {
                currWindow.setTruckLightColor(Color.BLACK);
            });

            try {
                Thread.sleep(sensorInterval * 500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            checkSensor();

            Platform.runLater(() -> {
                currWindow.setTruckLightColor(Color.WHITE);
            });

            try {
                Thread.sleep(sensorInterval * 500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (sensorOn) {
                onTime += sensorInterval;
                //System.out.println("SensorDriver.java: On: " + onTime);
                offTime = 0;
            } else if (!sensorOn) {
                onTime = 0;
                offTime += sensorInterval;
                //System.out.println("SensorDriver.java: Off: " + offTime);
            }

            if (onTime >= lagOn && !truckPresent) {
                truckPresent = true;
                startJob();
                updateModelChassis();
            } else if (offTime > lagOff && truckPresent) {
                truckPresent = false;
                endJob();
                nullModelChassis();
            }

            updateTime();
        }
        Thread.currentThread().interrupt();
    }

    /**
     * Method to updates startTime in StageWithData, also updates the light color calls method to update Model Chassis number
     *
     * @return the time the job started
     */
    @Override
    public LocalDateTime startJob() {

        LocalDateTime time = LocalDateTime.now(ZoneId.of("America/Montreal"));
        Platform.runLater(() -> {
            currWindow.setJobDone(false);
            currWindow.setStartTime(time);
            currWindow.setTruckLightColor(Color.GREEN);
        });

        return time;
    }

    /**
     * Method to update endTime in StageWithData, also updates the light color
     *
     * @return the time the job ended.
     */
    @Override
    public LocalDateTime endJob() {
        LocalDateTime time = LocalDateTime.now(ZoneId.of("America/Montreal"));
        Platform.runLater(() -> {
            currWindow.setJobDone(true);
            currWindow.setEndTime(time);
            currWindow.setTruckLightColor(Color.DARKRED);
        });
        return time;
    }

    /**
     * Validates the Model and Chassis number format. Come back to complete when we have more info
     *
     * @return boolean to determine whether input was valid.
     */

    public static boolean validateMC(String[] mc) {
        return true;
    }


    //Accessor and Mutator for TruckPresent()
    @Override
    public boolean isTruckPresent() {
        return truckPresent;
    }

    @Override
    public void setTruckPresent(boolean truckPresent) {
        this.truckPresent = truckPresent;
    }
}

