package com.zhuohong.he.volvo;

import boofcv.abst.fiducial.QrCodeDetector;
import boofcv.alg.fiducial.qrcode.QrCode;
import boofcv.factory.fiducial.FactoryFiducial;
import boofcv.gui.feature.VisualizeShapes;
import boofcv.gui.image.ShowImages;
import boofcv.struct.image.GrayU8;

import boofcv.io.UtilIO;
import boofcv.io.image.ConvertBufferedImage;
import boofcv.io.image.UtilImageIO;
import com.google.zxing.qrcode.encoder.QRCode;
import com.hopding.jrpicam.RPiCamera;
import com.hopding.jrpicam.enums.AWB;
import com.hopding.jrpicam.enums.DRC;
import com.hopding.jrpicam.enums.Encoding;

import java.awt.*;
import java.io.*;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.hopding.jrpicam.enums.Exposure;
import com.hopding.jrpicam.exceptions.FailedToRunRaspistillException;
import javafx.application.Platform;
import javafx.scene.Camera;
import javafx.util.Pair;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.util.Map;

import static java.awt.Color.PINK;


//*.*.* IMPORTANT *.*.*
//Make sure to install sudo raspi-config, select Enable camera and go to Finish.
// For more details look at: https://www.raspberrypi.org/documentation/usage/camera/raspicam/raspistill.md


public class CameraSensor {

    //Tunable Values
    final int rotation = 0;
    final boolean preview = false;
    final int time_out = 5;

    //Main method just used to test things.

    RPiCamera cam;
    MotorDriver m;
    StageWithData currWindow;
    /**
     * Main method is used to test. Reads all jpg files from the desktop.
     *
     * @param args
     */
    public static void main(String[] args) {
        File dir = new File("/users/Zooey He/Desktop/");
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".jpg");
            }
        });

        int boofRight = 0;
        int boofUnconfident = 0;
        int zxingRight = 0;

        for (File f : files) {
            System.out.println(f.getName());
            try {
                System.out.println("ZXing Result: " + readQRCode(f.getAbsolutePath()));
                zxingRight++;
            } catch (IOException e) {
                System.out.println("ZXing Result: IOException");
            } catch (NotFoundException e) {
                System.out.println("ZXing Result: NotFoundException");
            }
            Pair<List<QrCode>, List<QrCode>> boof = readBoof(UtilImageIO.loadImage(UtilIO.pathExample(f.getAbsolutePath())));

            if (boof.getKey().size() > 0) {
                System.out.println("BoofCV Result: " + boof.getKey().get(0).bounds.toString());
                boofRight++;
            } else if (boof.getValue().size() > 0) {
                System.out.println("BoofCV Result (U): " + boof.getValue().get(0).bounds.toString());
                boofUnconfident++;
            } else {
                System.out.println("BoofCV Result: Failed");
            }
            System.out.println();
        }

        System.out.println ("Test Done:");
        System.out.println ("-----------------------------------");
        System.out.println("Zxing Read\t\t" + zxingRight);
        System.out.println("Boof Confident\t\t" + boofRight);
        System.out.println("Boof Unconfident\t\t" + boofUnconfident);



        //CameraSensor camera = new CameraSensor();
        //System.out.println("Windshield Read: " + camera.scanWindshield());
    }


    public CameraSensor(StageWithData curr) {
        currWindow = curr;
        try {
            cam = new RPiCamera();
        } catch (FailedToRunRaspistillException e) {
            e.printStackTrace();
            System.err.println("No Camera Connected. Please try again.");
        }

        try {
            m = new MotorDriver(curr);
            m.setNeutral();
        } catch (Exception e) {
            System.err.println("Error with Motor, likely improper connection.");
        }
    }

    public String scanWindshield() throws QrNotFoundException{
        BufferedImage image;
        String result = "No Result";

        System.out.println("Camera called: Scanning WINDSHIELD");

        int triesCounter = 0;
        while (result.equals("No Result")) {
            triesCounter++;
            image = shootBufferedStill(cam);
            //try{Thread.sleep(1000);} catch (InterruptedException e) {
                //System.err.println("Error Interrupted during camera shot");
            //}
            try {
                result = readQRCode(image);
            } catch (IOException e) {
                System.out.println("IOException");
            } catch (NotFoundException e) {
                System.out.println("ZXing did not find");
                result = this.trackQR(cam, image);
            }

            if (triesCounter == 3) {
                System.out.println("Camera cannot sense. now write code to initiate manual entry");
                break;
            }
        }
        return result;
    }

    /**
     * TODO: Finish this so that it uses the motor to move the camera around: https://github.com/Pi4J/pi4j/blob/master/pi4j-example/src/main/java/PCA9685GpioServoExample.java
     * The purpose of this method is to position the camera in a better location
     * @return the resulting string determined from BoofCV at the best location.
     */
    public String trackQR(RPiCamera cam, BufferedImage image) throws QrNotFoundException{

        //1. This code does not move the camera
        //Replace the next two lines
        /*
        Pair<List<QrCode>, List<QrCode>> resultPair = readBoof(image);
        if (resultPair.getKey().size() == 0) {
            throw new QrNotFoundException();
        }
        return readBoof(image).getKey().get(0).message;
        */



        //2. First implementation used while loop
        Pair<List<QrCode>, List<QrCode>> scanResult;
        int count = 0;
        while((scanResult = readBoof(image)).getKey().size() == 0 || count == 0) {
            count ++;
            try {
                Platform.runLater(() -> {
                    currWindow.setTruckLightColor(javafx.scene.paint.Color.ORANGE);
                });
                m.adjust(scanResult, image.getWidth(), image.getHeight());
            } catch (QrNotFoundException e){
                try {
                    //The following is a bit challenging to comprehend. We will activate the adjust capability on the
                    //result of scanEnvironment. if QrNotFound is thrown by either adjust or scanEnvironment, we have
                    //to throw an exception to initiate manual input. Some stuff......
                    m.adjust(this.scanEnvironment(scanResult), image.getHeight(), image.getWidth());
                } catch (QrNotFoundException f) {
                    throw new QrNotFoundException();
                }
            }
            try {
                image = this.shootBufferedStill(cam);
                //Thread.sleep(1000);
            } catch (Exception e) {}
        }

        //3. Second implementation uses for loop
        /*
        for (int x = 0; x < 10; x++) {

            m.adjust(scanResult, image.getWidth(), image.getHeight());
            if (scanResult.getKey().size() != 0) {
                throw new QrNotFoundException();
            }
        }
        */
        return scanResult.getKey().get(0).message;

    }

    public Pair<List<QrCode>, List <QrCode>> scanEnvironment(Pair<List<QrCode>, List <QrCode>> curr) throws QrNotFoundException {
        Pair<List<QrCode>, List <QrCode>> result = curr;
        for (int x = 81; x <= 117; x += 18) {
            m.setPosition(1, x);
            for(int y = 72; y <= 108 ; y += 18) {
                m.setPosition(2, y);
                try{
                    BufferedImage img = this.shootBufferedStill(cam);
                    //Thread.sleep(1000);
                    result = readBoof(img);
                } catch (Exception e) {
                    System.err.println("Error Interrupted camera probing process.");
                }

                if (result.getKey().size() != 0 || result.getValue().size() != 0){
                    return result;
                }
            }
        }
        throw new QrNotFoundException();
    }

    public BufferedImage shootBufferedStill(RPiCamera piCamera) {
        piCamera.setAWB(AWB.AUTO);      // Change Automatic White Balance setting to automatic
        piCamera.setDRC(DRC.MEDIUM);            // Turn off Dynamic Range Compression
        piCamera.setContrast(100);           // Set maximum contrast
        piCamera.setSharpness(100);          // Set maximum sharpness
        piCamera.setQuality(100);            // Set maximum quality
        piCamera.setTimeout(time_out);           // Wait 1 second to take the image
        piCamera.setRotation(rotation);
        if (preview) {
            piCamera.turnOnPreview();
        } else {
            piCamera.turnOffPreview();
        }
        //piCamera.setAddRawBayer(true);
        //piCamera.setISO(200);
        //piCamera.setExposure(Exposure.ANTISHAKE);
        //piCamera.setColourEffect(128,128);
        //piCamera.setShutter(16666);         //Shutter speed of 1/60s, passed microsecond value
        //piCamera.turnOnPreview();            // Turn on image preview
        piCamera.setEncoding(Encoding.PNG); // Change encoding of images to PNG
        // Take a 650x650 still image, buffer it, and save it as "/home/pi/Desktop/A Cool Picture.png"
        BufferedImage buffImg = new BufferedImage(650, 650, BufferedImage.TYPE_INT_ARGB);
        try {
            buffImg = piCamera.takeBufferedStill(650, 650); // Take image and store in BufferedImage
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return buffImg;
    }

    public boolean shootBufferedStillandSave(RPiCamera piCamera, String fileLoc) {
        piCamera.setAWB(AWB.SUN);      // Change Automatic White Balance setting to automatic
        piCamera.setDRC(DRC.OFF);            // Turn off Dynamic Range Compression
        piCamera.setContrast(100);           // Set maximum contrast
        piCamera.setSharpness(100);          // Set maximum sharpness
        piCamera.setQuality(100);            // Set maximum quality
        piCamera.setTimeout(time_out);           // Wait 1 second to take the image
        if (preview) {
            piCamera.turnOnPreview();
        } else {
            piCamera.turnOffPreview();
        }
        //piCamera.turnOnPreview();            // Turn on image preview
        piCamera.setEncoding(Encoding.PNG); // Change encoding of images to PNG
        // Take a 650x650 still image, buffer it, and save it as "/home/pi/Desktop/A Cool Picture.png"
        piCamera.setRotation(0);
        BufferedImage buffImg = new BufferedImage(650, 650, BufferedImage.TYPE_INT_ARGB);
        try {
            buffImg = piCamera.takeBufferedStill(650, 650); // Take image and store in BufferedImage
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        File outfile = new File(fileLoc);
        try {
            return ImageIO.write(buffImg, "jpg", outfile);
        } catch (IOException e) {
            System.out.println("Error Writing Buffered Image");
            return false;
        }
    }

    public static String readQRCode(String filePath)
            throws FileNotFoundException, IOException, NotFoundException {
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new BufferedImageLuminanceSource(
                        ImageIO.read(new FileInputStream(filePath)))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap);
        return qrCodeResult.getText();
    }

    public static String readQRCode(BufferedImage qrImage)
            throws FileNotFoundException, IOException, NotFoundException {
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new BufferedImageLuminanceSource(qrImage)));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap);
        return qrCodeResult.getText();
    }

    /**
     * Uses BoofCV to read the QR codes in the photograph. It will return usefull information about
     * the location of the "QR Square" and the information in the QR Codes
     * @param input a buffered image of the picture w qr code
     * @return a Pair consisting of a list of successful QR reads as the key, and
     *              a list of partial reads as the value.
     */
    private static Pair<List<QrCode>, List<QrCode>> readBoof(BufferedImage input) {

        List<String> detectedList = new ArrayList<>();
        List<String> failedList = new ArrayList<>();

        GrayU8 gray = ConvertBufferedImage.convertFrom(input, (GrayU8) null);

        QrCodeDetector<GrayU8> detector = FactoryFiducial.qrcode(null, GrayU8.class);

        detector.process(gray);

        // Get's a list of all the qr codes it could successfully detect and decode
        List<QrCode> detections = detector.getDetections();

        Graphics2D g2 = input.createGraphics();
        int strokeWidth = Math.max(4, input.getWidth() / 200); // in large images the line can be too thin
        g2.setColor(Color.GREEN);
        g2.setStroke(new BasicStroke(strokeWidth));
        for (QrCode qr : detections) {
            // The message encoded in the marker
            //System.out.println("message: " + qr.message);

            // Visualize its location in the image
            VisualizeShapes.drawPolygon(qr.bounds, true, 1, g2);
            detectedList.add(qr.message);
        }

        // List of objects it thinks might be a QR Code but failed for various reasons
        List<QrCode> failures = detector.getFailures();
        g2.setColor(Color.RED);
        for (QrCode qr : failures) {
            // If the 'cause' is ERROR_CORRECTION or later then it's probably a real QR Code that
            if (qr.failureCause.ordinal() < QrCode.Failure.ERROR_CORRECTION.ordinal())
                continue;
            VisualizeShapes.drawPolygon(qr.bounds, true, 1, g2);
            failedList.add(qr.message);
        }

        ShowImages.showWindow(input, "Computer Vision", true);

        return new Pair<>(detections, failures);
    }
}

class QrNotFoundException extends Exception
{
    // Parameterless Constructor
    public QrNotFoundException() {}

    // Constructor that accepts a message
    public QrNotFoundException(String message)
    {
        super(message);
    }
}
