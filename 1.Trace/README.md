## TRACE STALL MANAGEMENT PROGRAM
**Volvo Group Internship - May 2018 to Aug 2018**

**Innovation and Emerging Technologies Intern - Macungie, PA**

Video of me explaining the project: (click the image for the video)

[![Youtube Interview](https://img.youtube.com/vi/gI2mdGqLNQs/3.jpg)](https://youtu.be/gI2mdGqLNQs?t=115)

I was chosen as one of four Innovation interns, who received support from the Volvo IT department to identify and resolve problems in the production process using emerging technologies.
For this internship, I designed a JavaFX application that runs on the Raspberry Pi 3B+. I built the project using several libraries and Maven repositories.

**The Problem:** The Volvo Truck production plant has a repair area at the end of the assembly line. This area is used to fix problems that are discovered on the line. Specialized repairmen tow trucks off the assembly line, load it into a repair area stall, and perform fixes. The management does not have metrics on how long different fixes take; therefore, they do not know how to schedule and improve efficiency of repairs.

**The Solution:** Each truck would come onto the line with a QR code containing ID numbers attached to the windshield. I created an IoT project that uses a LIDAR sensor to determine when a truck enters the stall, then activates a motorized camera to find the QR code on the windshield. Next, repair requirements on that truck are pulled from Microsoft Azure SQL database. As repairmen work from one task to the next, my application stores valuable time information in the database. I then wrote a AWS lambda function to perform analysis on the numbers daily. I was able to get average repair times.

The project used a Raspberry Pi 3B+ and was structured as seen below. 

![image](https://drive.google.com/uc?export=view&id=1OdTT6Fd_6hhvXEen947LvioYdmrh154A)

Since I wrote the code during my internship, I am only authorized to release snippets of my code.
*Therefore, the files in this folder do not form the complete project*

---

**Internship Results**
I presented my working prototype to three Vice Presidents, several directors, and countless Volvo employees at four sites on the east coast. I received positive feedback from the management team. I wrote a development plan to perfect and industrialize my project. The company is currently in the process of funding my plan.

If interested in details about my project proposal to management (development schedule, budget, explored technologies, conclusions, future development plans, etc). See the link below.

https://drive.google.com/file/d/1eP7ot-Az9yGoaGB5KMe9rk2oY6Jqqrdh/view?usp=sharing

I would like to thank my mentor at Volvo for funding for my prototype and helping me navigating the corporate environment.